

---
title: Hello
subtitle: Who's this dude?
comments: false
---

My name is Shaun Murphy. This page is hosted on Gitlab where I can make use of the CI/CD features to keep this page up-to-date. Important things you need to know about me:

- I have a Jack Russell
- I have three houseplants that have been alive for just under a year
- I make a mean lasagne


### My History

I've accumalated over two decades of experience whilst working my way up from helpdesk to systems / cloud engineer. I have a wealth of skills, of which there are too many to list here, so why don't you
head over to [my LinkedIn profile](https://www.linkedin.com/in/moifmurphy/) where you'll get much a better idea.
